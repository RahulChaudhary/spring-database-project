package com.mysql.example.demo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.example.demo.model.Employee;
import com.mysql.example.demo.repo.EmployeeRepository;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeRepository eRepo;
	
	@PostMapping("/addEmployee")
	public String addEmployee(@RequestBody Employee obj) {
		eRepo.save(obj);
		return "Saved";
	}
	
	@GetMapping("/showAllEmployee")
	public Iterable<Employee> showAllEmployee(){
		return eRepo.findAll();
	}

	// calling jsp
	@GetMapping("/addEmp")
	public String addEmp() {
		return "addEmployee.jsp";
	}
	/*
	@RequestMapping("/updateEmployeeByEno/{eno}")
	public String updateEmployeeByEno(@RequestBody Employee obj, String eno) {
		Optional<Optional<Employee>> emp = Optional.ofNullable(eRepo.findById(eno));
		if(!emp.isPresent()) {
			
		}
		return "Updated";
	}*/
	
}
