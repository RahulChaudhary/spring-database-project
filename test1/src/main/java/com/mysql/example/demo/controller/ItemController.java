package com.mysql.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mysql.example.demo.model.Item;
import com.mysql.example.demo.repo.ItemRepository;

@RestController
public class ItemController {

	@Autowired
	private ItemRepository itemRepo;
	
	@PostMapping("/add")
	public String addItem (@RequestBody Item obj) {
		itemRepo.save(obj);
		return "Saved";
	}
	
	@GetMapping("/test")
	public String test() {
		return "Working";
	}
}
