package com.mysql.example.demo.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.mysql.example.demo.model.Item;

@Repository
public interface ItemRepository extends CrudRepository<Item, Integer> {

}
